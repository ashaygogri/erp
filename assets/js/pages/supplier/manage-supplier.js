var TableDatatables = function(){
	var handleSupplierTable = function(){
		var manageSupplierTable = $("#manage-supplier-table");
		var baseURL = window.location.origin;
		var filePath = "/helper/routing.php";
		var oTable=manageSupplierTable.DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: baseURL+filePath,
				type: "POST",
				data: {
					"page": "manage_supplier"
				}
			},
			"lengthMenu":[
				[5,15,25,-1], //for pagination
				[5,15,25,"All"] //-1 will be shown as All
			],
			"order":[
				[1,"desc"]
			],
			"columnDefs":[
				{
					'orderable': false,
					'targets': [0,-1]//will remove the sorting opt from 0th and last column
				}
			]
		});

		manageSupplierTable.on('click','.edit',function(e){
			var id = $(this).data('id');
			$("#edit_supplier_id").val(id);
		});
		
		manageSupplierTable.on('click','.delete',function(e){
			var id = $(this).data('id');
			$("#delete_record_id").val(id);
		});
		new $.fn.dataTable.Buttons(oTable,{
			buttons:[
				'copy','csv','pdf'
			]
		});
		oTable.buttons().container().appendTo($('#export-buttons'));
	}
	return {
		//main function to handle all the datatables
		init: function(){
			handleSupplierTable();
		}
	}
}();
jQuery(document).ready(function(){
	TableDatatables.init();
	$('.yes').click(function(e){
		var baseURL = window.location.origin;
		var filePath = "/helper/routing.php";
		var id =$("#edit_supplier_id").val();
		var csrf_token = $("#csrf_token").val();
		$.ajax({
			url: baseURL+filePath,
			method: "POST",
			data: {
				"supplier_id": id,
				"fetch":"supplier",
				"csrf_token": csrf_token
			},
			dataType: "json",
			success: function(data){
			    console.log(data);
				redirectPost(baseURL+'/views/pages/edit-supplier.php',data);
			}
		})
	});
	function redirectPost(url, data) {
		var form = document.createElement('form');
		document.body.appendChild(form);
		form.method = 'post';
		form.action = url;
		for (var name in data) {
			var input = document.createElement('input');
			input.type = 'hidden';
			input.name = name;
			input.value = data[name];
			form.appendChild(input);
		}
		form.submit();
	}
});