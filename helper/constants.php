<?php
define('BASEURL',$di->get('config')->get('base_url'));
define('BASEASSETS',BASEURL ."assets/");
define('BASEPAGES', BASEURL ."views/pages/");

define('ADD_ERROR', 'add error');
define('ADD_SUCCESS', 'add success');
define('UPDATE_ERROR', 'update error');
define('UPDATE_SUCCESS', 'update success');
define('DELETE_ERROR', 'delete error');
define('DELETE_SUCCESS', 'delete success');
define('VALIDATION_ERROR','validation error');
