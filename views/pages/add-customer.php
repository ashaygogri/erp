<?php
  require_once __DIR__ ."/../../helper/init.php";
  $page_title = "QUICK ERP | Add Customer";
  $sidebarSection = 'customer';
  $sideBarSubSection = 'add';
  Util::createCSRFToken();
  $errors = "";
  $old = "";
  if(Session::hasSession('old'))
  {
    $old = Session::getSession('old');
    Session::unsetSession('old');
  }
  if(Session::hasSession('errors'))
  {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
  }

  // Util::dd(Session::getSession('csrf_token'));
?>
<!DOCTYPE html>
<html lang="en">

<head>


    <?php
  require_once __DIR__."/../includes/head-section.php";
?>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
    require_once __DIR__ ."/../includes/sidebar.php";?>

        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!--NAVBAR  Topbar -->
                <?php  require_once __DIR__ ."/../includes/navbar.php";?>
                <!-- NAVBAR End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between">
                        <h1 class="h3 b-4 text-gray-800">Add Customer</h1>
                        <a href="" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                            <i class="fas fa-list-ul fa-sm text-white"></i>Manage Customer
                        </a>
                    </div>

                    <div class="container-flid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card shadow mb-4">
                                    <div class="card-header">
                                        <!-- CARD HEADER -->
                                        <h6 class="m-0 font-weight-bold text-primary">
                                            <i class="fa fa-plus"></i>Add Customer
                                        </h6>
                                    </div>
                                    <!-- End of card header -->

                                    <!-- Card body -->
                                    <div class="card-body">
                                        <form action="<?= BASEURL?>helper/routing.php" method="POST" id="add-customer">
                                            <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="param">
                                                            <label for="first_name">First Name</label>
                                                            <input type="text" class="form-control <?= $errors!='' ? ($errors->has('first_name') ? 'error is-invalid' : ''):'';?>" name="first_name" id="first_name" placeholder="Enter first name" value="<?=$old != '' ?$old['first_name']: '';?>">
                                                            <?php
                            if($errors!="" && $errors->has('first_name')):
                              echo "<span class='error'> {$errors->first('first_names')}</span>";
                            endif;
                          ?>
                                                        </div>

                                                        <div class="param">
                                                            <label for="last_name">Last Name</label>
                                                            <input type="text" class="form-control <?= $errors!='' ? ($errors->has('last_name') ? 'error is-invalid' : ''):'';?>" name="last_name" id="last_name" placeholder="Enter last name" value="<?=$old != '' ?$old['last_name']: '';?>">
                                                            <?php
                            if($errors!="" && $errors->has('last_name')):
                              echo "<span class='error'> {$errors->first('last_names')}</span>";
                            endif;
                          ?>
                                                        </div>

                                                        <div class="param">
                                                            <label for="gst_no">GST Number</label>
                                                            <input type="text" class="form-control <?= $errors!='' ? ($errors->has('gst_no') ? 'error is-invalid' : ''):'';?>" name="gst_no" id="gst_no" placeholder="Enter gst number" value="<?=$old != '' ?$old['gst_no']: '';?>">
                                                            <?php
                            if($errors!="" && $errors->has('gst_no')):
                              echo "<span class='error'> {$errors->first('gst_no')}</span>";
                            endif;
                          ?>
                                                        </div>

                                                        <div class="param">
                                                            <label for="phone_no">Phone Number</label>
                                                            <input type="text" class="form-control <?= $errors!='' ? ($errors->has('phone_no') ? 'error is-invalid' : ''):'';?>" name="phone_no" id="phone_no" placeholder="Enter phone number" value="<?=$old != '' ?$old['phone_no']: '';?>">
                                                            <?php
                            if($errors!="" && $errors->has('phone_no')):
                              echo "<span class='error'> {$errors->first('phone_no')}</span>";
                            endif;
                          ?>
                                                        </div>

                                                        <div class="param">
                                                            <label for="email_id">Email id</label>
                                                            <input type="text" class="form-control <?= $errors!='' ? ($errors->has('email_id') ? 'error is-invalid' : ''):'';?>" name="email_id" id="email_id" placeholder="Enter Email ID" value="<?=$old != '' ?$old['email_id']: '';?>">
                                                            <?php
                            if($errors!="" && $errors->has('email_id')):
                              echo "<span class='error'> {$errors->first('email_id')}</span>";
                            endif;
                          ?>
                                                        </div>

                                                        <div class="param">
                                                            <label for="gender">Gender</label>
                                                            <input type="text" class="form-control <?= $errors!='' ? ($errors->has('gender') ? 'error is-invalid' : ''):'';?>" name="gender" id="gender" placeholder="Enter gender" value="<?=$old != '' ?$old['gender']: '';?>">
                                                            <?php
                            if($errors!="" && $errors->has('gender')):
                              echo "<span class='error'> {$errors->first('gender')}</span>";
                            endif;
                          ?>

                                                        </div>




                                                    </div>
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-primary" name="add_customer" value="Submit">
                                        </form>
                                    </div>
                                    <!-- End of card body -->

                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php  require_once __DIR__ ."/../includes/footer.php";?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php  require_once __DIR__ ."/../includes/scroll-to-top.php";?>

    <!-- Logout Modal-->


    <?php  require_once __DIR__ ."/../includes/core-scripts.php";?>

    <script src="<?=BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?=BASEASSETS;?>js/pages/customer/add-customer.js"></script>
</body>

</html>
