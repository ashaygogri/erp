<!-- SELECT products.id, products.name as product_name, products.specification, products.quantity, products.eoq_level, products.danger_level, category.name as category_name, GROUP_CONCAT(CONCAT(suppliers.first_name, " ",suppliers.last_name) SEPARATOR ' | ') as supplier_name, products_selling_rate.selling_rate,products_selling_rate.with_effect_from FROM products_selling_rate INNER JOIN (SELECT product_id, MAX(with_effect_from) as wef FROM (SELECT product_id,with_effect_from FROM products_selling_rate WHERE with_effect_from<= CURRENT_TIMESTAMP) as temp GROUP BY temp.product_id) as max_date_table ON max_date_table.product_id = products_selling_rate.product_id AND products_selling_rate.with_effect_from = max_date_table.wef INNER join products ON products.id = products_selling_rate.product_id INNER JOIN category ON category.id = products.category_id INNER JOIN product_supplier ON product_supplier.product_id = products.id INNER JOIN suppliers ON suppliers.id = product_supplier.supplier_id WHERE products.deleted = 0 GROUP BY products.id -->

<?php
  require_once __DIR__ ."/../../helper/init.php";
  $page_title = "QUICK ERP | Manage Product";
  $sidebarSection = 'product';
  $sideBarSubSection = 'manage';
  Util::createCSRFToken();
?>
<!DOCTYPE html>
<html lang="en">

<head>

 
<?php
  require_once __DIR__."/../includes/head-section.php";
?>
<link rel="stylesheet" href="<?=BASEASSETS;?>css/plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="<?=BASEASSETS;?>vendor/datatables/datatables.min.css">


</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php
    require_once __DIR__ ."/../includes/sidebar.php";?>

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!--NAVBAR  Topbar -->
        <?php  require_once __DIR__ ."/../includes/navbar.php";?>
        <!-- NAVBAR End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
            <h1 class="h3 mb-4 text-gray-800">Manage Product</h1>
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Categories</h6>
              </div>
              <div id="export-buttons"></div>
              <div class="card-body">
                <table class="table table-bordered table-responsive" id="manage-product-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Product Name</th>
                      <th>Specifications</th>
                      <th>Quantity</th>
                      <th>Selling Rate</th>
                      <th>WEF</th>
                      <th>Category</th>
                      <th>EOQ</th>
                      <th>Danger Level</th>
                      <th>Supplier Names</th>
                      <th>Actions</th>

                    </tr>
                  </thead>
                </table>
              </div>
            </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="<?=BASEURL;?>helper/routing.php">
      <div class="modal-body">
        <input type="hidden" name="csrf_token" id="csrf_token" value="<?=Session::getSession('csrf_token');?>">
        <input type="hidden" name="record_id" id="delete_record_id">
        <p class="text-muted">Are you sure you want to delete this record?</p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger" name="deleteProduct">Delete Record!</button>
      </div>
      </form>
    </div>
  </div>
</div>
      <!-- /DELETE MODAL -->
      <!-- Footer -->
      <?php  require_once __DIR__ ."/../includes/footer.php";?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <?php  require_once __DIR__ ."/../includes/scroll-to-top.php";?>

  <!-- Logout Modal-->


  <?php  require_once __DIR__ ."/../includes/core-scripts.php";?>

<?php require_once __DIR__ . "/../includes/page-level/product/manage-product-scripts.php";?>

</body>

</html>
