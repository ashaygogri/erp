<?php
  require_once __DIR__ ."/../../helper/init.php";
  $page_title = "QUICK ERP | Edit Customer";
  $sidebarSection = 'customer';
  $sideBarSubSection = 'edit';
  if(Session::hasSession('old')){
    $result = Session::getSession('old');
    $customer_id = $result['customer_id'];
  }
  $customer = $di->get('database')->readData('customers',[],'deleted=0 AND id= '.$customer_id);
    Util::createCSRFToken();
    $first_name = $customer[0]->first_name;
    $last_name = $customer[0]->last_name;
    $gst_no = $customer[0]->gst_no;
    $phone_no = $customer[0]->phone_no;
    $email_id = $customer[0]->email_id;
    $gender = $customer[0]->gender;

?>
<!DOCTYPE html>
<html lang="en">

<head>


    <?php
  require_once __DIR__."/../includes/head-section.php";
?>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
    require_once __DIR__ ."/../includes/sidebar.php";?>

        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!--NAVBAR  Topbar -->
                <?php  require_once __DIR__ ."/../includes/navbar.php";?>
                <!-- NAVBAR End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between">
                        <h1 class="h3 b-4 text-gray-800">Edit Customer</h1>
                        <a href="" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                            <i class="fas fa-list-ul fa-sm text-white"></i>Manage Customer
                        </a>
                    </div>

                    <div class="container-flid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card shadow mb-4">
                                    <div class="card-header">
                                        <!-- CARD HEADER -->
                                        <h6 class="m-0 font-weight-bold text-primary">
                                            <i class="fa fa-plus"></i>Edit Customer
                                        </h6>
                                    </div>
                                    <!-- End of card header -->

                                    <!-- Card body -->
                                    <div class="card-body">
                                        <form action="<?= BASEURL?>helper/routing.php" method="POST" id="edit-customer">
                                            <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                                            <input type="hidden" name="customer_id" id="edit_customer_id" value="<?=$customer_id?>"> 
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="param">
                                                            <label for="first_name">First Name</label>
                                                            <input type="text" name="first_name" id="first_name" class="form_control" value="<?=$first_name?>">
                                                        </div>
                                                       
                                                        <div class="param">
                                                            <label for="last_name">Last Name</label>
                                                            <input type="text" name="last_name" id="last_name" class="form_control" value="<?=$last_name?>">
                                                        </div>

                                                        <div class="param">
                                                            <label for="gst_no">GST Number</label>
                                                            <input type="text" name="gst_no" id="gst_no" class="form_control" value="<?=$gst_no?>">
                                                        </div>

                                                        <div class="param">
                                                            <label for="phone_no">Phone Number</label>
                                                            <input type="text" name="phone_no" id="phone_no" class="form_control" value="<?=$phone_no?>">
                                                        </div>

                                                        <div class="param">
                                                            <label for="email_id">Email id</label>
                                                            <input type="text" name="email_id" id="email_id" class="form_control" value="<?=$email_id?>">
                                                        </div>

                                                        <div class="param">
                                                            <label for="gender">Gender</label>
                                                            <input type="text" name="gender" id="gender" class="form_control" value="<?=$gender?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-primary" name="editCustomer" value="Submit">
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php  require_once __DIR__ ."/../includes/footer.php";?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php  require_once __DIR__ ."/../includes/scroll-to-top.php";?>

    <!-- Logout Modal-->


    <?php  require_once __DIR__ ."/../includes/core-scripts.php";?>

    <script src="<?=BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?=BASEASSETS;?>js/pages/customer/edit-customer.js"></script>
</body>

</html>
