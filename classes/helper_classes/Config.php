<?php

class Config
{
    protected $config;
    /**
     * Config constructor
     */

     public function __construct()
     {
        $this->config = parse_ini_file(__DIR__ ."/../../config.ini");
     }
     public function get(string $key):string
     {
         if(isset($this->config[$key]))
         {
             return $this->config[$key];
         }

         die('This config cannot be found ' .$key);
     }
}