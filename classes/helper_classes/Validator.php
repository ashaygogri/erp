<?php

class Validator
{
    private $di;
    protected $database;
    protected $errorHandler;

    protected $rules = ["required", "minlength", "maxlength", "unique", "email"];

    protected $messages = [
        "required" => "This :field field is required",
        "minlength" => "The :field must be a minimum of :satisfier characters",
        "maxlength" => "The :field must be a maximum of :satisfier characters",
        "email" => "This is not a valid email address",
        "unique" => "That :field is already taken"
    ];

    /**
     * Validator constructor.
     * @param Database $database
     * @param ErrorHandler $errorHandler
     */
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
        $this->errorHandler = $this->di->get('error_handler');
    }
    public function check($items, $rules,$id=0)
    {
        foreach($items as $item=>$value)
        {
            if(in_array($item, array_keys($rules)))
            {
                $this->validate([
                    'field'=>$item,
                    'value'=>$value,
                    'rules'=>$rules[$item]
                ],$id);
            }
        }
        return $this;
    }
    public function fails()
    {
        return $this->errorHandler->hasErrors();
    }
    public function errors()
    {
        return $this->errorHandler;
    }
    private function validate($item,$id=0)
    {
        // die(var_dump($item));
        /**
         * $item['field'] -> contains the column name which has to be tested for the validation
         * $item['value'] -> contains value which was inserted by the user in the form
         * $item['rules'] -> it is array of rules to be applied for the specific 'field'
         */

        $field = $item['field'];
        $value = $item['value'];
        foreach ($item['rules'] as $rule=>$satisfier)
        {
            if(!call_user_func_array([$this, $rule], [$field, $value, $satisfier,$id]))
            {
                // Store the error in the error handler
                $this->errorHandler->addError(str_replace([':field', ':satisfier'], [$field, $satisfier], $this->messages[$rule]), $field);
            }
        }
    }

    private function required($field, $value, $satisfier,$id=0)
    {
        return !empty(trim($value));
    }
    private function minlength($field, $value, $satisfier,$id=0)
    {
        return mb_strlen(trim($value))>=$satisfier;
    }
    private function maxlength($field, $value, $satisfier,$id=0)
    {
        return mb_strlen(trim($value))<=$satisfier;
    }
    private function unique($field, $value, $satisfier,$id=0)
    {
        // Here $satisfier will become the name of table!
        // $field will become the name of column
        // $value should be unique under the above table and column
        return !$this->database->exists($satisfier,[$field=>$value],$id);
    }
    private function email($field, $value, $satisfier,$id=0)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

}

?>